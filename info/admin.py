# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import City, OriginAddress, DestinationAddress, RouteInformation

# Register your models here.
admin.site.register(City)
admin.site.register(OriginAddress)
admin.site.register(DestinationAddress)


class RouteInformationAdmin(admin.ModelAdmin):
    readonly_fields = ['datetime', 'distance', 'duration_in_traffic', 'city',
                       'origin_address', 'destination_address']


admin.site.register(RouteInformation, RouteInformationAdmin)
