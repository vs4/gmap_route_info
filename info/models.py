# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone


class City(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return "{class_name}(id={id})(name={name})".format(class_name=self.__class__.__name__,  # NOQA
                                                           id=self.id,
                                                           name=self.name
                                                           )


class OriginAddress(models.Model):
    city = models.ForeignKey(City,
                             related_name='origin_addresses')
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return "{class_name}(id={id})(name={name})".format(class_name=self.__class__.__name__,  # NOQA
                                                           id=self.id,
                                                           name=self.name
                                                           )


class DestinationAddress(models.Model):
    city = models.ForeignKey(City,
                             related_name='destinations_addresses')
    origin_address = models.ForeignKey(OriginAddress,
                                       related_name='destinations')
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return "{class_name}(id={id})(name={name})".format(class_name=self.__class__.__name__,  # NOQA
                                                           id=self.id,
                                                           name=self.name
                                                           )


class RouteInformation(models.Model):
    datetime = models.DateTimeField(default=timezone.now())
    distance = models.IntegerField()  # meters
    duration_in_traffic = models.CharField(max_length=100)
    city = models.ForeignKey(City, related_name='routes')
    origin_address = models.ForeignKey(OriginAddress, related_name='routes')
    destination_address = models.ForeignKey(DestinationAddress,
                                            related_name='routes')
