
from models import OriginAddress, RouteInformation
from utils import get_route_information
from celery.decorators import periodic_task
from celery.task.schedules import timedelta
# from celery import shared_task
# from celery.decorators import task


@periodic_task(run_every=timedelta(hours=1),
               ignore_result=True, name='test_task')
def get_all_durations_and_distance():
    # some long running task here
    origins = OriginAddress.objects.all()
    for origin in origins:
        for destination in origin.destinations.all():
            info = get_route_information(city_name=origin.city.name,
                                         origin_name=origin.name,
                                         destination_name=destination.name)

            if info:
                distance = info.get('distance')
                duration_in_traffic = info.get('duration_in_traffic')
                if distance and duration_in_traffic:
                    RouteInformation.objects.create(city=origin.city,
                                           distance=distance['value'],
                                           duration_in_traffic=duration_in_traffic['text'],  # NOQA
                                           origin_address=origin,
                                           destination_address=destination)
