import googlemaps
from django.conf import settings
import logging


def get_route_information(city_name='Kiev',
                          origin_name=None,
                          destination_name=None):
    if settings.GOOGLE_MAP_KEY and origin_name and destination_name:
        gmap = googlemaps.Client(key=settings.GOOGLE_MAP_KEY)
        origin_name = origin_name.replace(' ', '+') + ',' + city_name
        destination_name = destination_name.replace(' ', '+') + ',' + city_name
        r = gmap.distance_matrix(origins=origin_name,
                                 destinations=destination_name,
                                 units='metric',
                                 departure_time='now',
                                 mode='driving',
                                 traffic_model='best_guess')
        if r['rows'][0]['elements'][0]['status'] == 'OK':
            return r['rows'][0]['elements'][0]
        else:
            logging.error('One of address is wrong: origin_name or destination_name') # NOQA
            return False
    else:
        logging.error('One of this credentials is empty: GOOGLE_MAP_KEY, origin_name or destination_name') # NOQA
        return False
